<html>
    <head>
        <title><?php echo $title ?></title>
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="<?php echo $router->generate('registration'); ?>">Rejestracja</a></li>
                <li><a href="<?php echo $router->generate('homepage') ?>">Homepage</a></li>
                <li><a href="<?php echo $router->generate('article', ['id' => 1]) ?>">Article1</a></li>
            </ul>
        </nav>
        <h1>Hello world</h1>
        <?php include 'parts/flash-messages.php' ?>
        <?php if ($session->has('user')): ?>
            <p>Witaj, <?php echo $session->get('user') ?></p>

            <p><a href="<?php echo $router->generate('logout') ?>">Wyloguj</a></p>
        <?php else: ?>
            <form action="<?php echo $router->generate('login_check') ?>" method="post">
                <input name="username" value="" placeholder="Username" />
                <input name="password" type="password" value="" placeholder="Password" />

                <button type="submit">Zaloguj</button>
            </form>
        <?php endif ?>

        <?php echo $content; ?>
    </body>
</html>