<?php

namespace App;

use App\Controllers\ControllerInterface;
use App\Exception\InvalidRouteConfigurationException;
use App\Exception\PageNotFoundException;
use App\Exception\ServerErrorException;
use App\Response\ErrorResponse;
use App\Response\LayoutResponse;
use App\Session\Session;

class App
{
    private $request;

    /**
     * Uruchamia aplikację
     */
    public function run(): void
    {
        $this->request = Request::fromGlobals();

        $router = $this->getRouter();
        try {
            $matchedRoute = $router->match($this->request);
            $response = $matchedRoute($this->request);
        } catch (PageNotFoundException $exception) {
            $response = new ErrorResponse($router, $exception, 404);
        } catch (ServerErrorException $exception) {
            $response = new ErrorResponse($router, $exception, 500);
        }

        foreach ($response->getHeaders() as $header) {
            header($header);
        }

        echo $response->getBody();
    }

    /**
     * @return Router
     * @throws \Exception
     */
    public function getRouter()
    {
        return ServiceContainer::getInstance()->get('router');
    }
}