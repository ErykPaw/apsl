<?php

namespace App\Controllers;

use App\Request;
use App\Response\LayoutResponse;
use App\Response\Response;
use App\Router;

class RegistrationController implements ControllerInterface
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function __invoke(Request $request): Response
    {
        // TODO: Implement __invoke() method.
        return new LayoutResponse('registration', [
            'request' => $request,
            'router' => $this->router
        ]);
    }
}