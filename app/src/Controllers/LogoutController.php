<?php


namespace App\Controllers;

use App\Request;
use App\Response\RedirectResponse;
use App\Response\Response;
use App\Router;
use App\Session\Session;

class LogoutController implements ControllerInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Router
     */
    private $router;

    /**
     * LoginCheckController constructor.
     * @param Session $session
     * @param Router $router
     */
    public function __construct(Session $session, Router $router)
    {
        $this->session = $session;
        $this->router = $router;
    }

    public function __invoke(Request $request): Response
    {
        $this->session->destroy();

        return new RedirectResponse(
            $this->router->generate('homepage')
        );
    }
}