<?php


namespace App\Controllers;

use App\Model\UserCredentials;
use App\Repository\UserRepositoryInterface;
use App\Request;
use App\Response\RedirectResponse;
use App\Response\Response;
use App\Router;
use App\Security\PasswordEncoderInterface;
use App\Session\Session;

class LoginCheckController implements ControllerInterface
{
    const POST_USERNAME = 'username';
    const POST_PASSWORD = 'password';

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * @var PasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * LoginCheckController constructor.
     * @param Session $session
     * @param Router $router
     * @param UserRepositoryInterface $repository
     * @param PasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        Session $session,
        Router $router,
        UserRepositoryInterface $repository,
        PasswordEncoderInterface $passwordEncoder
    ) {
        $this->session = $session;
        $this->router = $router;
        $this->repository = $repository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Request $request): Response
    {
        $response = new RedirectResponse(
            $this->router->generate('homepage')
        );

        if (!$request->isPost() || !$request->hasPost(self::POST_USERNAME) || !$request->hasPost(self::POST_PASSWORD)) {
            return $response;
        }

        $username = $request->getPost(self::POST_USERNAME);
        $plainPassword = $request->getPost(self::POST_PASSWORD);
        $credentials = $this->repository->findCredentialsByUsername($username);

        if ($credentials && $this->validatePassword($credentials, $plainPassword)) {
            $this->session->regenerate();
            $this->session->set('user', $credentials->getUsername());
            $this->session->setFlashMessage('success', 'Zostałeś zalogowany do systemu.');
        } else {
            $this->session->setFlashMessage('error', 'Niepoprawne dane.');
        }

        return $response;
    }

    /**
     * @param UserCredentials $credentials
     * @param string $plainPassword
     * @return bool
     */
    private function validatePassword(UserCredentials $credentials, string $plainPassword)
    {
        return $this->passwordEncoder->encodePassword($plainPassword) === $credentials->getPassword();
    }
}