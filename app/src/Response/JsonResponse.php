<?php

namespace App\Response;

class JsonResponse extends Response
{
    public function __construct($data, $headers = [], $status = 200)
    {
        parent::__construct(
            json_encode($data),
            array_merge([
                'Content-Type: application/json'
            ], $headers),
            $status
        );
    }
}