<?php

namespace App;

use App\Controllers\LoginCheckController;
use App\Controllers\LogoutController;
use App\Controllers\PageController;
use App\Controllers\RegistrationController;
use App\Repository\ChainUserRepository;
use App\Repository\CsvFileUserRepository;
use App\Repository\InMemoryUserRepository;
use App\Repository\JsonFileUserRepository;
use App\Repository\PdoUserRepository;
use App\Security\Sha1PasswordEncoder;
use App\Session\Session;

class ServiceContainer
{
    private static $instance;

    /**
     * @var array
     */
    private $factories = [];

    /**
     * @var array
     */
    private $services = [];

    private function __construct()
    {
        $this->factories['router'] = function (ServiceContainer $container) {
            $router = new Router();

            $router->addRoute('homepage', [
                'path' => '/',
                'controller' => function () use ($router) {
                    return new PageController($router, 'homepage');
                }
            ]);

            $router->addRoute('article', [
                    'path' => '/article/{id}',
                    'controller' => function () use ($router) {
                        return new PageController($router, 'article');
                    }
            ]);

            $router->addRoute('invalid', [
                'path' => '/invalid'
            ]);

            $router->addRoute('login_check', [
                'path' => '/login-check',
                'controller' => function () use ($container) {
                    return new LoginCheckController(
                        $container->get('session'),
                        $container->get('router'),
                        $container->get('user_repository'),
                        $container->get('password_encoder')
                    );
                }
            ]);

            $router->addRoute('logout', [
                'path' => '/logout',
                'controller' => function () use ($container) {
                    return new LogoutController(
                        $container->get('session'),
                        $container->get('router')
                    );
                }
            ]);

            $router->addRoute('registration', [
                'path' => '/registration',
                'controller' => function () use ($router) {
                    return new RegistrationController($router);
                }
            ]);

            return $router;
        };

        $this->factories['session'] = function () {
            return new Session();
        };

        $this->factories['password_encoder'] = function () {
            return new Sha1PasswordEncoder();
        };

        $this->factories['user_repository'] = function ($container) {
            $repository = new ChainUserRepository();
            $repository->addRepository(new PdoUserRepository('sqlite:///' . __DIR__ . '/../data/users.db'));
            $repository->addRepository(new JsonFileUserRepository(__DIR__ . '/../data/users.json'));
            $repository->addRepository(new CsvFileUserRepository(__DIR__ . '/../data/users.csv'));
            $repository->addRepository(InMemoryUserRepository::createFromPlainPasswords($container->get('password_encoder'), [
                'test5' => 'test123',
                'test6' => 'test123'
            ]));

            return $repository;
        };
    }

    /**
     * @return ServiceContainer
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        if (!$this->has($id)) {
            throw new \Exception(sprintf('Service "%s" not defined', $id));
        }

        if (!isset($this->services[$id])) {
            $this->services[$id] = $this->factories[$id]($this);
        }

        return $this->services[$id];
    }

    /**
     * @param $id
     * @return bool
     */
    public function has($id)
    {
        return isset($this->factories[$id]);
    }
}