<?php

namespace App;

class Layout
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $page;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $params;

    /**
     * @param Request $request
     * @param string $page
     * @param string $name
     */
    public function __construct(string $page, string $name, array $params)
    {
        $this->page = $page;
        $this->name = $name;
        $this->params = $params;
    }

    public function render(): string
    {
        extract(array_merge($this->params, [
            'content' => $this->renderPage(),
            'session' => ServiceContainer::getInstance()->get('session')
        ]));

        ob_start();
        include __DIR__ . "/../layouts/{$this->name}.php";

        return ob_get_clean();
    }

    private function renderPage()
    {
        ob_start();
        extract($this->params);
        include __DIR__ . "/../templates/{$this->page}.php";

        return ob_get_clean();
    }
}